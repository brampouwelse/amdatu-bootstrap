/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.console.impl;

import java.util.Properties;

import org.amdatu.bootstrap.core.api.Prompt;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
	
		
		dm.add(createComponent().setInterface(new String[] {Prompt.class.getName(), CommandSessionListener.class.getName()}, null).setImplementation(ConsolePrompt.class));
		
		String[] topics = new String[] {
				"org/amdatu/bootstrap/core/*"
	        };
	        
        Properties props = new Properties();
        props.put(EventConstants.EVENT_TOPIC, topics);
		dm.add(createComponent().setInterface(EventHandler.class.getName(), props).setImplementation(CoreEventHandlers.class));
	}
}
