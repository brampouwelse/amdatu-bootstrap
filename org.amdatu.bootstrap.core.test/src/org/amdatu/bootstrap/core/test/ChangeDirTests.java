package org.amdatu.bootstrap.core.test;

import java.nio.file.Path;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.testframework.AbstractBaseTestCase;

public class ChangeDirTests extends AbstractBaseTestCase{
	
	@Override
	protected void setUp() throws Exception {
		addServiceDependencies(Navigator.class);
		super.setUp();
	}
	
	public void testMoveDirUp() throws Exception {
		runScript("core:cd ..");
		Path currentDir = m_navigator.getCurrentDir();
		assertEquals("build/ws", currentDir.toString());
	}
	
	public void testMoveToProject() throws Exception {
		m_navigator.createProject("example");
		assertEquals("build/ws/testWorkspace/example", m_navigator.getCurrentDir().toString());
		
		runScript("core:cd ..");
		assertEquals("build/ws/testWorkspace", m_navigator.getCurrentDir().toString());
		
		runScript("core:cd example");
		assertEquals("build/ws/testWorkspace/example", m_navigator.getCurrentDir().toString());
	}
	
	public void testToHomeDir() throws Exception {
		runScript("core:cd");
		Path currentDir = m_navigator.getCurrentDir();
		assertEquals(System.getProperty("user.home"), currentDir.toString());
	}
}
