/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugin.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that denotes a method as being a command for use in shells, such as Felix Gogo.
 * <p>
 * All methods annotated with <tt>@Command</tt> inside a {@link Plugin} are considered to be accessible as shell command.
 * This allows plugins to provide more fine-grained functionality to the user.
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface Command {
    /**
     * @return the (optional) description for the command, as shown in the help of the shell.
     */
    String description() default "";
    /** @return a list of interface types that can be prompted, used for command completion. */
    Class[] types() default {};
}
