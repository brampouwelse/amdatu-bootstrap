/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.jackson;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.properties.Document;

@Component(provides = { Plugin.class})
public class JacksonPlugin extends AbstractBasePlugin{

	private final static String[] OLD_BUNDLES = {"jackson-core-asl", "jackson-mapper-asl", "net.vz.mongodb.jackson.mongo-jackson-mapper", "de.undercouch.bson4jackson", "jackson-jaxrs"};
	private final static String[] OLD_MAPPER_BUNDLES = { "net.vz.mongodb.jackson.mongo-jackson-mapper", "de.undercouch.bson4jackson"};
	
	private final Map<String, String> newJacksonBundles;
	private final Map<String, String> newMapperBundles;
	
	@ServiceDependency
	private volatile Prompt m_prompt;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	public JacksonPlugin() {
		newJacksonBundles = new HashMap<>();
		newJacksonBundles.put("com.fasterxml.jackson.core.jackson-annotations", "2.3.0");
		newJacksonBundles.put("com.fasterxml.jackson.core.jackson-core", "2.3.1");
		newJacksonBundles.put("com.fasterxml.jackson.core.jackson-databind", "2.3.1");
		
		newMapperBundles = new HashMap<>();
		newMapperBundles.put("org.mongojack", "2.1.0.SNAPSHOT");
		newMapperBundles.put("de.undercouch.bson4jackson", "2.3.1");
	}
	
	@Override
	public String getName() {
		return "jackson";
	}

	@Override
	public boolean isInstalled() {
		return true;
	}
	
	@Command
	public void upgradeJackson() {
		boolean upgradeWorkspace = m_prompt.askBoolean("Do you want to upgrade Jackson on the whole workspace?", false);
		
		if(upgradeWorkspace) {
			Path workspaceDir = m_navigator.getWorkspaceDir();
			File[] projectDirs = workspaceDir.toFile().listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.isDirectory();
				}
			});
			
			for (File projectDir : projectDirs) {
				try {
					m_navigator.getCurrentWorkspace();
					m_navigator.changeDir(projectDir.toPath());
					
					BndEditModel model = new BndEditModel(); 
					Path bndfile = m_navigator.getBndFile();
					model.loadFrom(bndfile.toFile());
					Document document = new Document(new String(Files.readAllBytes(bndfile)));

					updateBuildPath(model, bndfile, document);
					updateRunConfig(model, bndfile, document);
					
					System.out.println("Updated project " + m_navigator.getCurrentProject());
					
				} catch (Exception e) {
					System.out.println("Skipping directory " + projectDir + " because it doesn't look like a project");
				}
			}
		}
	}

	private void updateBuildPath(BndEditModel model, Path bndfile, Document document) {
		Iterator<VersionedClause> iterator = model.getBuildPath().iterator();
		List<VersionedClause> newBuildPath = new ArrayList<>();

		boolean addJacksonBundles = false;
		boolean addMapperBundles = false;
		
		while(iterator.hasNext()) {
			VersionedClause vc = iterator.next();
			
			if(!shouldReplace(vc.getName())) {
				newBuildPath.add(vc);
			} else {
				
				addJacksonBundles = true;
				
				if(isMapper(vc.getName())) {
					addMapperBundles = true;
				}
			}
		}
		
		model.setBuildPath(newBuildPath);
		model.saveChangesTo(document);
		String string = document.get();
		m_resourceManager.writeFile(bndfile, string.getBytes());
		
		if(addJacksonBundles) {
			for (String bundle : newJacksonBundles.keySet()) {
				m_dependencyBuilder.addDependency(bundle, newJacksonBundles.get(bundle));
			}
		}
		
		if(addMapperBundles) {
			for (String bundle : newMapperBundles.keySet()) {
				m_dependencyBuilder.addDependency(bundle, newMapperBundles.get(bundle));
			}
		}
	}
	
	private void updateRunConfig(BndEditModel model, Path bndfile, Document document) {
		Iterator<VersionedClause> iterator = model.getRunBundles().iterator();
		List<VersionedClause> newRunConfig = new ArrayList<>();

		boolean addJacksonBundles = false;
		boolean addMapperBundles = false;

		while(iterator.hasNext()) {
			VersionedClause vc = iterator.next();
			
			if(!shouldReplace(vc.getName())) {
				newRunConfig.add(vc);
			} else {
				
				addJacksonBundles = true;
				
				if(isMapper(vc.getName())) {
					addMapperBundles = true;
				}
			}
		}
		
		model.setRunBundles(newRunConfig);
		model.saveChangesTo(document);
		String string = document.get();
		m_resourceManager.writeFile(bndfile, string.getBytes());
		
		if(addJacksonBundles) {
			for (String bundle : newJacksonBundles.keySet()) {
				m_dependencyBuilder.addRunDependency(bundle, newJacksonBundles.get(bundle));
			}
		}
		
		if(addMapperBundles) {
			for (String bundle : newMapperBundles.keySet()) {
				m_dependencyBuilder.addRunDependency(bundle, newMapperBundles.get(bundle));
			}
		}

	}

	private boolean shouldReplace(String bundleSymbolicName) {
		for (String bsn : OLD_BUNDLES) {
			if(bsn.equals(bundleSymbolicName)) {
				return true;
			}
		}
		
		return false;
	}

	private boolean isMapper(String bundleSymbolicName) {
		for (String bsn : OLD_MAPPER_BUNDLES) {
			if(bsn.equals(bundleSymbolicName)) {
				return true;
			}
		}
		
		return false;
	}
}
