/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.navigator;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component(provides = Plugin.class)
public class ShellNavigator extends AbstractBasePlugin {
    
    @ServiceDependency
    private Prompt m_prompt;
    
    @ServiceDependency
    private Navigator m_navigator;

    @Override
    public String getName() {
        return "ShellNavigator";
    }
    
    @Command(description = "Short hand for navigate.")
    public void nav(String... args) {
        navigate(args);
    }
    
    @Command(description = "A shell friendly navigator, it prompts you on what folder to move to. Args will serve as a filter.")
    public void navigate(final String... args) {
        do {
            System.out.println("In " + m_navigator.getCurrentDir());
            File[] listFiles = m_navigator.getCurrentDir().toFile().listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (!pathname.isDirectory()) {
                        return false;
                    }
                    if (args.length > 0) {
                        for (int i = 0; i < args.length; i++) {
                            if (!pathname.getName().contains(args[i])) {
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
            List<File> options = new ArrayList<>();
            options.add( m_navigator.getCurrentDir().getParent().toFile());
            options.addAll(Arrays.asList(listFiles));
            File choice = m_prompt.askChoice("Navigate to what dir? (no option to exit)", -1, options);
            if (choice == null) {
                // user wants to exit
                return;
            }
            m_navigator.changeDir(choice.toPath());
        } while (true && args.length == 0);
    }

    /*******************************************************************
     * This plugin can't be installed as not something you can install *
     *******************************************************************/

    @Override
    public boolean isProjectInstallAllowed() {
        return false;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return false;
    }

    @Override
    public boolean isInstalled() {
        return true;
    }

}
