/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.testframework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import junit.framework.TestCase;

import org.amdatu.bootstrap.core.api.Navigator;
import org.apache.commons.io.FileUtils;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.apache.felix.service.command.CommandSession;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class AbstractBaseTestCase extends TestCase {
	private volatile CommandProcessor m_commandProcessor;
	protected volatile BundleContext m_bundleContext;
	protected DependencyManager dependencyManager;
	private CountDownLatch countDownLatch;
	private List<Class<?>> serviceDependencies = new ArrayList<Class<?>>();
	protected volatile Navigator m_navigator;
	private CommandSession m_session;

	protected void setUp() throws Exception {
		FileUtils.deleteDirectory(new File("build/ws/testWorkspace"));
		
		Path buildDirPath = Paths.get("build");
		if(!buildDirPath.toFile().exists()) {
			Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxr-x---");
			FileAttribute<Set<PosixFilePermission>> attr = PosixFilePermissions.asFileAttribute(perms);
			Files.createDirectory(buildDirPath, attr);
		}
		m_bundleContext = FrameworkUtil.getBundle(AbstractBaseTestCase.class).getBundleContext();
		URL buildFile = m_bundleContext.getBundle().getEntry("templates/cnf.zip");

		try (InputStream in = buildFile.openStream()) {
			Path cnfPath = Paths.get("build", "cnf.zip");
			Files.copy(in, cnfPath, StandardCopyOption.REPLACE_EXISTING);
			File zipFile = cnfPath.toFile();
			unzipFile(zipFile, Paths.get("build/ws", "testWorkspace").toFile());
			zipFile.delete();
		}

		dependencyManager = new DependencyManager(m_bundleContext);
		countDownLatch = new CountDownLatch(serviceDependencies.size());

		addServiceDependencies(CommandProcessor.class, Navigator.class);

		Component component = dependencyManager.createComponent().setImplementation(this);

		for (Class<?> serviceClass : serviceDependencies) {
			component.add(dependencyManager.createServiceDependency().setService(serviceClass).setRequired(true).setAutoConfig(true)
					.setCallbacks("serviceInjected", "serviceRemoved"));
		}

		dependencyManager.add(component);

		boolean created = countDownLatch.await(10, TimeUnit.SECONDS);
		if (!created) {
			fail("Service instance could not be injected");
		}
		
		m_navigator.changeDir(Paths.get("build/ws/testWorkspace"));
		
		runScript("SCOPE = core:*");
	}

	protected void runScript(String script) throws Exception {
		m_session = m_commandProcessor.createSession(System.in, System.out, System.err);
		m_session.execute(script);
		m_session.close();
	}

	protected void runScript(File scriptFile) throws Exception {
		byte[] fileBytes = Files.readAllBytes(scriptFile.toPath());
		runScript(new String(fileBytes, Charset.defaultCharset()));
	}

	@SuppressWarnings("unused")
	private void serviceInjected() {
		countDownLatch.countDown();
	}
	
	@Override
	protected void tearDown() throws Exception {
		//m_session.close();
	}

	protected void addServiceDependencies(Class<?>... services) {
		for (Class<?> clazz : services) {
			serviceDependencies.add(clazz);
		}
	}

	protected void unzipFile(File fileToUnzip, File toLocation) {
		byte[] buffer = new byte[1024];

		// create output directory is not exists
		if (!toLocation.exists()) {
			toLocation.mkdir();
		}
		// get the zip file content
		try (ZipInputStream zis = new ZipInputStream(new FileInputStream(fileToUnzip))) {
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(toLocation + File.separator + fileName);

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				if (ze.isDirectory()) {
					// create the folder
					newFile.mkdirs();
				} else {
					// extract file
					try (FileOutputStream fos = new FileOutputStream(newFile)) {
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
					}
				}

				ze = zis.getNextEntry();
			}

			zis.closeEntry();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
